
import 'dart:async';

import 'package:koodawaala/blocs/bloc.dart';
import 'package:koodawaala/controllers/api_repository.dart';
import 'package:koodawaala/controllers/api_response.dart';
import 'package:koodawaala/models/demo_response.dart';




class HomeBloc implements Bloc{
  ApiRepository _apiRepository;

  StreamController _homeDataController;

  StreamSink<ApiResponse<DemoResponse>> get homeDataSink =>
      _homeDataController.sink;

  Stream<ApiResponse<DemoResponse>> get homeDataStream =>
      _homeDataController.stream;

  HomeBloc() {
    _homeDataController = StreamController<ApiResponse<DemoResponse>>();
    _apiRepository = ApiRepository();
    fetchData();
  }

  fetchData() async {
      homeDataSink.add(ApiResponse.loading('Fetching Home Data.'));
    try {
      DemoResponse data = await _apiRepository.getHomeData();
      homeDataSink.add(ApiResponse.completed(data));
    } catch (e) {
      homeDataSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }


  dispose() {
    _homeDataController?.close();
  }
}