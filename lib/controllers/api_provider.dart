import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:koodawaala/controllers/api_exceptions.dart';
import 'package:http/http.dart' as http;
import 'package:koodawaala/utils/constants.dart';

/*
For making communication between our Remote server and Application we use various APIs which needs some type of HTTP methods to get executed.
So we are first going to create a base API helper class, which will be going to help us communicate with our server.
*/



class ApiProvider {
  final String _baseUrl = AppConstants.BASE_URL;

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(_baseUrl + url);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException('Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}