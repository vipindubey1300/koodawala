import 'package:koodawaala/controllers/api_provider.dart';
import 'package:koodawaala/models/demo_response.dart';
import 'package:koodawaala/utils/constants.dart';

/*
We are going to use a Repository class which going to act as the inter-mediator and a layer of abstraction between the APIs and the BLOC.
 The task of the repository is to deliver movies data to the BLOC after fetching it from the API.
*/


class ApiRepository {

  ApiProvider _helper = ApiProvider();


  Future<DemoResponse> getHomeData() async {
    final response = await _helper.get('jokes/random?category=political');
    var res = DemoResponse.fromJson(response);
    return res ;
  }
}