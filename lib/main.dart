import 'package:flutter/material.dart';


//customs
import 'utils/app_colors.dart';
import 'blocs/application_bloc.dart';
import 'blocs/bloc_provider.dart';


//screens
import 'screens/demo_home.dart';
import 'screens/login.dart';
import 'screens/main_tab.dart';
import 'screens/profile.dart';

final ThemeData defaultTheme = new ThemeData(
  primaryColor: Colors.white,
  accentColor: Colors.green,
  brightness: Brightness.light,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);


/*
  With these lines, we simply instantiate a new BlocProvider which will handle a ApplicationBloc, and will render the MyApp as a child.
  From that moment on, any widget part of the sub-tree, starting at BlocProvider will be able to get access to the ApplicationBloc,
  via the following line:
    (ApplicationBloc bloc = BlocProvider.of<ApplicationBloc>(context);)
 */
/*
Place a ApplicationBloc BLoC above the material app to store the app’s state.
Application blocs should be broadcast
 */

void main() => runApp(
    BlocProvider<ApplicationBloc>(
      bloc: ApplicationBloc(),
      child: MyApp(),
    )
);



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: defaultTheme,
      //home: Splash(),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/':  (context) => new Login(),
        '/main_tab':  (context) => new MainTab(),
        '/profile':  (context) => new Profile(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}


