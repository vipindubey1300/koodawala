import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/styles.dart';

import 'package:koodawaala/widgets/address_item.dart';
import 'package:koodawaala/widgets/apartment_item.dart';
import 'package:koodawaala/widgets/custom_button.dart';
import 'package:koodawaala/widgets/custom_mapview.dart';

class Apartment {
  String name;
  String price;
  int id;

  Apartment({
    this.name,
    this.price,
    this.id,
  });
}

enum SubscriptionType { oneTime, subscription }


class Cleaning extends StatefulWidget {
  Cleaning({Key key}) : super(key: key);

  @override
  _CleaningState createState() => _CleaningState();
}

class _CleaningState extends State<Cleaning>{

  List<Apartment> itemData;
  List<int> items = [];
  int _selectedItem = 0;
  String address ;
  void selectItem(index) {
    setState(() => _selectedItem = index);
  }


  @override
  void initState() {
    super.initState();
    setState(() {
      itemData = [
        Apartment( name: '1 BHK', id: 1,price: '₹ 70'),
        Apartment( name: '2 BHK', id: 2 ,price: '₹ 80'),
        Apartment( name: '3 BHK', id: 3,price: '₹ 90'),
        Apartment( name: '4 BHK', id: 4 ,price: '₹ 100'),
      ];
    });
  }

  TextStyle headingStyle = new TextStyle(color:Colors.black,fontWeight: FontWeight.w700,fontSize: 15);
  TextStyle smallHeadingStyle = new TextStyle(color:Colors.black,fontWeight: FontWeight.w300,fontSize: 12);




  SubscriptionType _character = SubscriptionType.oneTime;





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false, // this avoids the overflow error
      body:SafeArea(
        child:  Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.all(10),
            child:  SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 30.0),
                 // Image.asset('assets/images/logo.png', width: 300, height: 150, fit:BoxFit.contain),
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: AppStyles.boxDecoration,
                    child: Row(
                      children: [
                        Expanded(flex: 3,
                          child: new Container(
                              clipBehavior: Clip.antiAlias, //prevent overflow
                              height: 150.0,
                              decoration:  AppStyles.boxDecoration,
                              child: GestureDetector(
                                child:  Image.asset('assets/images/avatar.jpg', width: 300, height: 150, fit:BoxFit.cover),
                                onTap: () =>  Navigator.pushNamed(context,'/profile'),
                              )
                          ),),
                        Expanded(flex: 7,
                            child:Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Punit Baba Jee',style: headingStyle,),
                                  SizedBox(height: 15.0),
                                  Text('Email',style: headingStyle,),
                                  SizedBox(height: 5.0),
                                  Text('baba@gmail.com',style: smallHeadingStyle,),
                                  SizedBox(height: 15.0),
                                  Text('Phone No.',style: headingStyle,),
                                  SizedBox(height: 5.0),
                                  Text('234532434324',style: smallHeadingStyle,),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        padding: EdgeInsets.all(8.0),
                                        decoration: new BoxDecoration(
                                          borderRadius: BorderRadius.all(const Radius.circular(20.0)),
                                          border: Border.all(color:  Colors.grey),
                                        ),
                                        child: GestureDetector(
                                          child:  Image.asset('assets/images/edit.png', width: 20, height: 20, fit:BoxFit.cover),
                                        ),
                                      )
                                  )

                                ],
                              ),
                            )
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15.0),
                  CustomMapView(onGetAddress: (addr) => address = addr),
                  SizedBox(height: 15.0),
                  Container(
                    decoration: AppStyles.boxDecoration,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Text('Select Subscription Type',style: headingStyle,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(child:   ListTile(
                              title: const Text('One Time'),
                              leading: Radio(
                                activeColor: AppColors.PRIMARY_COLOR,
                                value: SubscriptionType.oneTime,
                                groupValue: _character,
                                onChanged: (SubscriptionType value) {

                                  setState(() {
                                    _character = value;
                                  });
                                },
                              ),
                            )),
                            Expanded(child:  ListTile(
                              title: const Text('Subscription'),
                              leading: Radio(
                                activeColor: AppColors.PRIMARY_COLOR,
                                value: SubscriptionType.subscription,
                                groupValue: _character,
                                onChanged: (SubscriptionType value) {
                                  setState(() {
                                    _character = value;
                                  });
                                },
                              ),
                            )),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                      padding: EdgeInsets.all(10),
                      decoration: AppStyles.boxDecoration,
                      child:Column(
                        children: [
                          Text('Select Apartment Type',style: headingStyle,),
                          SizedBox(height: 10,),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: itemData.length > 0 ? itemData.length : 0,
                              itemBuilder: (context, index) => ApartmentItem(
                                selectItem, // callback function, setstate for parent
                                index: index,
                                isSelected: _selectedItem == index ? true : false,
                                title:itemData[index].name,
                                price:itemData[index].price ,
                              )
                          )
                        ],
                      )),
                  SizedBox(height: 15.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('*Currently we offer only weekly cleaning services.',
                        style: TextStyle(height: 3, fontSize: 15,),
                      ),
                      Text('*Our Cleaning Partner will arrive daily at 8:00 AM at your location.',
                        style: TextStyle(height: 3, fontSize: 15),
                      ),
                      Text('*Service is for make clean India green India Campaign.',
                        style: TextStyle(height: 3, fontSize: 15),
                      )
                    ],
                  ),

                  SizedBox(height: 15.0),
                  CustomButton(text: 'Clean',onPressed: () => print(address.toString()),),
                  SizedBox(height: 15.0),






                ],
              ),
            )
        ),
      ),
    );
  }
}
