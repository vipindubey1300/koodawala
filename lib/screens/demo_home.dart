import 'package:flutter/material.dart';
import 'package:koodawaala/blocs/bloc_provider.dart';
import 'package:koodawaala/blocs/demo_bloc.dart';
import 'package:koodawaala/controllers/api_response.dart';
import 'package:koodawaala/models/demo_response.dart';
//import 'package:upgrader/upgrader.dart';

class DemoHome extends StatefulWidget {
  DemoHome({Key key}) : super(key: key);

  @override
  _DemoHomeState createState() => _DemoHomeState();
}

class _DemoHomeState extends State<DemoHome> {


  HomeBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = HomeBloc();
  }

  @override
  Widget build(BuildContext context) {
      return BlocProvider<HomeBloc>(
        bloc: _bloc,
        child: new Scaffold(
            resizeToAvoidBottomInset: false,
            //key: _scaffoldKey,
            body: new SafeArea(
              child:showBody(_bloc),
            )
        ),
      );
    }
  Widget showBody(HomeBloc _bloc){
   return RefreshIndicator(
      onRefresh: () => _bloc.fetchData(),
      child: StreamBuilder<ApiResponse<DemoResponse>>(
        stream: _bloc.homeDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Loading(loadingMessage: snapshot.data.message);
                break;
              case Status.COMPLETED:
              // return CategoryList(categoryList: snapshot.data.data);
                return Text("Loaded data ",style: TextStyle(color: Colors.red),);
                break;
              case Status.ERROR:

                return Error(
                  errorMessage: snapshot.data.message,
                  onRetryPressed: () => _bloc.fetchData(),
                );

                break;
            }
          }
          return Container();
        },
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

}


class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black45,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.black45),
          ),
        ],
      ),
    );
  }
}

class Error extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const Error({Key key, this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          RaisedButton(
            color: Colors.white,
            child: Text('Retry', style: TextStyle(color: Colors.black)),
            onPressed: onRetryPressed,
          )
        ],
      ),
    );
  }
}
