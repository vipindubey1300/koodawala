import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:koodawaala/screens/garbage_pickup.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/styles.dart';
import 'package:koodawaala/utils/utility.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:koodawaala/widgets/address_item.dart';
import 'package:koodawaala/widgets/custom_button.dart';
import 'package:koodawaala/widgets/custom_checkbox.dart';
import 'package:koodawaala/widgets/custom_mapview.dart';
import 'package:koodawaala/widgets/global/google_map.dart';
import 'package:geocoder/geocoder.dart';
import 'package:koodawaala/widgets/list_tile.dart';
import 'package:koodawaala/widgets/quantity.dart';


class ItemData {
  String name;
  int id;

  ItemData({
    this.name,
    this.id,
  });
}


class Donate extends StatefulWidget {
  Donate({Key key}) : super(key: key);

  @override
  _DonateState createState() => _DonateState();
}

class _DonateState extends State<Donate>{

  bool _disposed = false;
  List<ItemData> itemData;
  int _selectedItem = 0;
  String _selectedDate;

  void selectItem(index) => setState(() => _selectedItem = index);

  @override
  void initState()  {
    super.initState();
    if (!_disposed)
     print("Screen focused");

    setState(() {
      itemData = [
        ItemData( name: 'Clothes', id: 1,),
        ItemData( name: 'Shoes', id: 2 ),
        ItemData( name: 'BedSheet', id: 3,),
        ItemData( name: 'Blankets', id: 4 ),
      ];
    });

  }

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
  }
  callback(newAbc) {
    print(newAbc);
  }


  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2000),
        lastDate: new DateTime(2050)
    );
    if(picked != null) {
      String result = Utility.dateFormatter(picked);
      print(result);
      setState(() => _selectedDate =result);
    }

  }

  TextStyle headingStyle = new TextStyle(color:Colors.black,fontWeight: FontWeight.w700,fontSize: 15);
  TextStyle smallHeadingStyle = new TextStyle(color:Colors.black,fontWeight: FontWeight.w300,fontSize: 12);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false, // this avoids the overflow error
      body:SafeArea(
        child:  Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.all(10),
            child:  SingleChildScrollView(
             child: Column(
               children: [
                 SizedBox(height: 15,),
                  CustomMapView(),
                 SizedBox(height: 15,),



                Align(alignment: Alignment.centerLeft,
                  child:  Text('*Currently we offer only weekly cleaning services.',
                  style: TextStyle(height: 3, fontSize: 15,),
                ),),
                SizedBox(height: 15,),

                 Container(
                     padding: EdgeInsets.all(10),
                     width: double.infinity,
                     decoration: AppStyles.boxDecoration,
                     child:Column(
                       children: [
                         Text('Select Building Type',style: headingStyle,),
                         SizedBox(height: 10,),
                         ListView.builder(
                             physics: NeverScrollableScrollPhysics(),
                             shrinkWrap: true,
                             itemCount: itemData.length > 0 ? itemData.length : 0,
                             itemBuilder: (context, index) => ListTileItem(
                               selectItem, // callback function, setstate for parent
                               index: index,
                               isSelected: _selectedItem == index ? true : false,
                               title:itemData[index].name,
                             )
                         )
                       ],
                     )),
                 SizedBox(height: 15.0),


                 Container(
                     padding: EdgeInsets.all(10),
                     width: double.infinity,
                     decoration: AppStyles.boxDecoration,
                     child:Column(
                       children: [
                         Text('Pick A Date',style: headingStyle,),
                         SizedBox(height: 10,),
                         GestureDetector(
                           onTap: ()=> _selectDate(),
                           child: Container(
                               decoration: AppStyles.boxDecoration,
                               padding: EdgeInsets.all(10),
                               child:  Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: [
                                   if(_selectedDate == null) Text('Select Date') else Text(_selectedDate,style: headingStyle,),
                                   Icon( Icons.calendar_today)
                                 ],
                               ),

                           ),
                         ),

                       ],
                     )),
                 SizedBox(height: 15.0),


                 SizedBox(height: 15.0),
                 CustomButton(text: 'Donate')


               ],
             ),
            )
        ),
      ),
    );
  }
}
