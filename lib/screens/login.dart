import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/utility.dart';
import 'package:koodawaala/widgets/custom_button.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {


  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    //rest code here
  }

  @override
  void dispose() {
    //rest code here
    super.dispose();
  }

  Widget getNameInput() => Container(
    width: Utility.getDeviceWidth(context) * 0.77,
    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
    child: TextFormField(
      textAlign: TextAlign.center,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(16.0),
          filled: true,
          fillColor: Colors.white,
          hintText: 'Name',
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          )
      ),
      validator: (value) =>  value.isEmpty ? 'Name can\'nt be empty' : null,
      onSaved: (name) {
        print(name);
      },
    ),
  );

  Widget getPhoneInput() => Container(
    width: Utility.getDeviceWidth(context) * 0.77,
    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
    child: TextFormField(
      textAlign: TextAlign.center,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(16.0),
          filled: true,
          fillColor: Colors.white,
          hintText: 'Phone Number',
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          )
      ),
      validator: (value) => value.isEmpty ? 'Phone Number can\'nt be empty' : null,
      onSaved: (phone) {
        debugPrint('$phone');
        print(phone);
      },
    ),
  );

  Widget getPasswordInput() => Container(
    width: Utility.getDeviceWidth(context) * 0.77,
    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
    child: TextFormField(
      textAlign: TextAlign.center,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(16.0),
          filled: true,
          fillColor: Colors.white,
          hintText: 'Password',
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.7),
          )
      ),
      obscureText: true,
      validator: (value) => value.isEmpty ? 'Password can\'nt be empty' : null,
      onSaved: (password) {
        debugPrint('$password');
        print(password);
      },
    ),
  );

  void onCustomButtonPressed(){
    Navigator.pushNamed(context,'/main_tab');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false, // this avoids the overflow error
      body:SafeArea(
        child:  Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.all(10),
            child:  SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 30.0),
                  Image.asset('assets/images/illustration.png', width: 300, height: 150, fit:BoxFit.contain),
                  Image.asset('assets/images/logo.png', width: 300, height: 150, fit:BoxFit.contain),
                  Form(
                      child: Column(children: <Widget>[
                        getNameInput(),
                        SizedBox(height: 10.0),
                        getPhoneInput(),
                        SizedBox(height: 10.0),
                        getPasswordInput(),
                        SizedBox(height: 20.0),
                        Container(
                         width: Utility.getDeviceWidth(context) * 0.7,
                         child:  Text("*We don't store any of the information until you make booking or subscribe"),
                       ),
                        SizedBox(height: 80.0),
                        CustomButton(text: 'Delete Trash From Your Life',  onPressed: onCustomButtonPressed,)
                      ])),


                ],
              ),
            )
      ),
      ),
    );
  }
}
