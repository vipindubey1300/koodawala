import 'package:flutter/material.dart';
import 'package:koodawaala/screens/cleaning.dart';
import 'package:koodawaala/screens/demo_home.dart';
import 'package:koodawaala/screens/donate.dart';
import 'package:koodawaala/utils/app_colors.dart';
import './garbage_pickup.dart';

class MainTab extends StatefulWidget {
  MainTab({Key key}) : super(key: key);

  @override
  _MainTabState createState() => _MainTabState();
}

class _MainTabState extends State<MainTab> {
  PageController pageController = PageController();
  int _currentIndex = 0;
  final List<Widget> _children = [
       Cleaning(),
       GarbagePickup(),
       Donate(),
  ];


  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });

  }
  void _onItemTapped(int index){
    pageController.jumpToPage(index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Container(
                alignment: Alignment.topCenter,
                child: Container(
                  constraints: BoxConstraints(minWidth: 200, maxWidth: 500),
                  child: PageView(
                    controller: pageController,
                    onPageChanged: onTabTapped ,
                    children: _children,
                    physics: NeverScrollableScrollPhysics(),
                  ),
                )
            ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0x00ffffff), // transparent
        onTap:_onItemTapped , // new
        currentIndex: _currentIndex, // new
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: new Image.asset('assets/images/cleaning.png',width: 20,height: 20,),
              activeIcon:new Image.asset('assets/images/cleaning-selected.png',width: 25,height: 25,),
              label: 'Cleaning'
          ),
          BottomNavigationBarItem(
               icon: new Image.asset('assets/images/garbage-pickup.png',width: 20,height: 20,),
               activeIcon:new Image.asset('assets/images/garbage-pickup-selected.png',width: 25,height: 25,),
              label: 'Garbage Pickup'
          ),
          BottomNavigationBarItem(
              icon: new Image.asset('assets/images/donation.png',width: 20,height: 20,),
              activeIcon:new Image.asset('assets/images/donation-selected.png',width: 25,height: 25,),
              label: 'Donate'
          ),

        ],
        type: BottomNavigationBarType.fixed,
        fixedColor:AppColors.PRIMARY_COLOR,
      ),

      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }



}
