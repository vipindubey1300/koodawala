import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/styles.dart';
import 'package:koodawaala/utils/utility.dart';
import 'package:koodawaala/widgets/back_header.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile>{



  @override
  void initState()  {
    super.initState();
    //rest code here
    Utility.setStatusBar();
    

  }


  @override
  void dispose() {
    //rest code here
    super.dispose();
  }

  Widget getGridData(int index)  => Container(
    decoration: AppStyles.boxDecoration,
    child: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(padding: EdgeInsets.all(5),child: Text('23/0/2021',style: AppStyles.bigText,),),
        Padding(padding: EdgeInsets.all(5),child: Text("₹ 100",style: AppStyles.largeText,),),
        Column(
          children: [
           if(index % 2 == 0) Text('Monthly Charge',style: TextStyle(color:Colors.orange),) else Container(),
            SizedBox(height: 5,),
            Container(
              height: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: (index % 2 == 0) ?  Colors.green : Colors.orange
              ),
              child: Center(
                  child: Text("Cleaning",style: AppStyles.blackSmallText,)
              ),
            )
          ],
        )
      ],
    ),
  );

  

  @override
  Widget build(BuildContext context) {
    return   Scaffold(
      backgroundColor:AppColors.PRIMARY_COLOR_TRANSPARENT,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Positioned.fill(
                child: Container(
                  color: Colors.white,
                  child:   SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      Container(
                        clipBehavior: Clip.antiAlias,
                        height: Utility.getDeviceHeight(context) * 0.4,
                        width: double.infinity,
                        decoration: new BoxDecoration(
                          color: Colors.grey,
                          borderRadius: new BorderRadius.only(
                            bottomLeft: const Radius.circular(40.0),
                            bottomRight: const Radius.circular(40.0),
                          )
                      ),
                        child:   Image.asset('assets/images/avatar.jpg',fit: BoxFit.fill,),
                      ),

                     Align(
                       alignment: Alignment.topCenter,
                       child:  Container(
                         transform: Matrix4.translationValues(0.0, -40.0, 0.0),
                         width: Utility.getDeviceWidth(context) * 0.78,
                         color: Colors.transparent,
                         child: SizedBox(
                           width: double.infinity, //here it  means match parent height
                           child: Card(
                               shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.circular(20.0),
                               ),
                               child:  Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Container(
                                     decoration: BoxDecoration(
                                       borderRadius: BorderRadius.circular(20),
                                       color: Colors.white,
                                       boxShadow: [
                                         BoxShadow(color: Colors.grey, spreadRadius: 0.1),
                                       ],
                                     ),
                                     padding: EdgeInsets.all(10),
                                     child: Row(
                                       mainAxisSize: MainAxisSize.max,
                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                       children: [
                                         Text('Punit Babe Jee',style: TextStyle(fontWeight: FontWeight.w700,fontSize: 22,color: Colors.green),),
                                         Container(
                                           padding: EdgeInsets.all(8.0),
                                           decoration: new BoxDecoration(
                                             borderRadius: BorderRadius.all(const Radius.circular(20.0)),
                                             border: Border.all(color:  Colors.grey),
                                           ),
                                           child: GestureDetector(
                                             child:  Image.asset('assets/images/edit.png', width: 20, height: 20, fit:BoxFit.cover),
                                           ),
                                         )
                                       ],
                                     ),
                                   ),
                                   SizedBox(height: 30,),
                                   Container(
                                     padding: EdgeInsets.all(12),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         Text('Phone',style: AppStyles.smallText,),
                                         Text('+91232323234234',style: AppStyles.bigText,),
                                         SizedBox(height: 18,),
                                         Text('Email',style: AppStyles.smallText,),
                                         Text('baba@gmail.com',style: AppStyles.bigText,),
                                         SizedBox(height: 35,),
                                         Container(
                                           decoration: BoxDecoration(border: Border(top: BorderSide(width: 0.5, color: Colors.grey,))),
                                           padding: EdgeInsets.all(15),
                                           child: Row(
                                             children: [
                                               Expanded(
                                                   flex:1,
                                                   child: Container(
                                                     decoration: BoxDecoration(border: Border(right: BorderSide(width: 0.2, color: Colors.grey,))),
                                                     height: 60,
                                                     child: Center(
                                                       child: Column(
                                                         mainAxisSize: MainAxisSize.min,
                                                         children: [
                                                           Text('Donations Made',style: AppStyles.greenBigText,),
                                                           Text('230',style: AppStyles.bigText,)
                                                         ],
                                                       ),
                                                     ),
                                                   )
                                               ),
                                               Expanded(
                                                   flex:1,
                                                   child: Container(
                                                       decoration: BoxDecoration(border: Border(left: BorderSide(width: 0.2, color: Colors.grey,))),
                                                       height: 60,
                                                       child: Center(
                                                         child: Column(
                                                           mainAxisSize: MainAxisSize.min,
                                                           children: [
                                                             Text('Smiles Delivered ',style: AppStyles.greenBigText,),
                                                             Text('30',style: AppStyles.bigText,)
                                                           ],
                                                         ),
                                                       )
                                                   )
                                               )
                                             ],
                                           ),
                                         )

                                       ],
                                     ),
                                   )
                                 ],
                               )
                           ),
                         ),
                       ),
                     ),

                     Container(
                       padding: EdgeInsets.all(16),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Text('Transactions History',style: AppStyles.blackSmallText,),
                           SizedBox(height: 18,),
                           GridView.builder(
                             physics: NeverScrollableScrollPhysics(),
                             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, crossAxisSpacing: 8, mainAxisSpacing: 4,
                             // childAspectRatio: (Utility.getDeviceWidth(context) / 2) / (Utility.getDeviceHeight(context) / 2),
                             ),
                             padding: EdgeInsets.all(8),
                             shrinkWrap: true,
                             itemCount: 4,
                             itemBuilder: (context, index) {
                               return getGridData(index);
                             },
                            )

                         ],
                       ),
                     )

                    ],),
                  ),
                )
            ),
            Positioned(child:BackHeader(text: "Profile"),top: 0,left: 0,right: 0,),
          ],
        ),
      ),
    );
  }
}
