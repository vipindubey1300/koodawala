/**
    // Don't
    import 'package:demo/src/utils/dialog_utils.dart';

    // Do
    import '../../../utils/dialog_utils.dart';

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    var item = 10;
    final car = Car();
    const timeOut = 2000;

    //Do
    int item = 10;
    final Car bar = Car();
    String name = 'john';
    const int timeOut = 20;

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    (item as Animal).name = 'Lion';


    //Do
    if (item is Animal)
    item.name = 'Lion'
    ----------------------------------------------------------------------------------------------------------------

    //Don't
    Widget getText(BuildContext context) {
    return Row(
    children: [
    Text("Hello"),
    Platform.isAndroid ? Text("Android") : null,
    Platform.isAndroid ? Text("Android") : SizeBox(),
    Platform.isAndroid ? Text("Android") : Container(),
    ]
    );
    }


    //Do
    Widget getText(BuildContext context) {
    return Row(
    children:
    [
    Text("Hello"),
    if (Platform.isAndroid) Text("Android")
    ]
    );
    }

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    v = a == null ? b : a;

    //Do
    v = a ?? b;


    //Don't
    v = a == null ? null : a.b;

    //Do
    v = a?.b;

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    var y = [4,5,6];
    var x = [1,2];
    x.addAll(y);


    //Do
    var y = [4,5,6];
    var x = [1,2,...y]

    ----------------------------------------------------------------------------------------------------------------
    // Don't
    var path = Path();
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();


    // Do
    var path = Path()
    ..lineTo(0, size.height)
    ..lineTo(size.width, size.height)
    ..lineTo(size.width, 0)
    ..close();

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    var s = 'This is demo string \\ and \$';

    //Do
    var s = r'This is demo string \ and $';

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    int _item = null;

    //Do
    int _item;

    ----------------------------------------------------------------------------------------------------------------
    //Don't
    get width {
     return right - left;
    }

    Widget getProgressBar() {
    return CircularProgressIndicator(
    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
    );
    }


    //Do
    get width => right - left;
    Widget getProgressBar() => CircularProgressIndicator(
    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
    );

    ----------------------------------------------------------------------------------------------------------------
    Split widget into different Widgets. ->

    Scaffold(
    appBar: CustomAppBar(title: "Verify Code"), // Sub Widget
    body: Container(
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
    TimerView( // Sub Widget
    key: _timerKey,
    resendClick: () {})
    ],
    ),
    ),
    )

 **/



/**
 * createState(): When the Framework is instructed to build a StatefulWidget, it immediately calls createState()

    mounted is true: When createState creates your state class, a buildContext is assigned to that state. buildContext is, overly simplified, the place in the widget tree in which this widget is placed. Here's a longer explanation. All widgets have a bool this.mounted property. It is turned true when the buildContext is assigned. It is an error to call setState when a widget is unmounted.

    initState(): This is the first method called when the widget is created (after the class constructor, of course.) initState is called once and only once. It must call super.initState().

    didChangeDependencies(): This method is called immediately after initState on the first time the widget is built.

    build(): This method is called often. It is required, and it must return a Widget.

    didUpdateWidget(Widget oldWidget): If the parent widget changes and has to rebuild this widget (because it needs to give it different data), but it's being rebuilt with the same runtimeType, then this method is called. This is because Flutter is re-using the state, which is long lived. In this case, you may want to initialize some data again, as you would in initState.

    setState(): This method is called often from the framework itself and from the developer. Its used to notify the framework that data has changed

    deactivate(): Deactivate is called when State is removed from the tree, but it might be reinserted before the current frame change is finished. This method exists basically because State objects can be moved from one point in a tree to another.

    dispose(): dispose() is called when the State object is removed, which is permanent. This method is where you should unsubscribe and cancel all animations, streams, etc.

    mounted is false: The state object can never remount, and an error is thrown is setState is called.
**/


/**
 *Emit the data to parent Widget in Flutter------------->
 *
 * void main() => runApp(new TestApp());

    class TestApp extends StatefulWidget {
    @override
    _TestState createState() => new _TestState();
    }

    class _TestState extends State<TestApp> {
    String abc = "bb";

    callback(newAbc) {
    setState(() {
    abc = newAbc;
    });
    }

    @override
    Widget build(BuildContext context) {
    var column = new Column(
    children: <Widget>[
    new Text("This is text $abc"),
    TestApp2(abc, callback)
    ],
    );
    return new MaterialApp(
    home: new Scaffold(
    body: new Padding(padding: EdgeInsets.all(30.0), child: column),
    ),
    );
    }
    }

    class TestApp2 extends StatefulWidget {
    String abc;
    Function(String) callback;

    TestApp2(this.abc, this.callback);

    @override
    _TestState2 createState() => new _TestState2();
    }

    class _TestState2 extends State<TestApp2> {
    @override
    Widget build(BuildContext context) {
    return new Container(
    width: 150.0,
    height: 30.0,
    margin: EdgeInsets.only(top: 50.0),
    child: new FlatButton(
    onPressed: () {
    widget.callback("RANDON TEXT"); //call to parent
    },
    child: new Text(widget.abc),
    color: Colors.red,
    ),
    );
    }
    }*/


/**
 * CALING CHILD COMPONENT FUNCTION FROM PARENT ---
 *
 * class MyWidget extends StatefulWidget {
    @override
    State<StatefulWidget> createState() => _MyWidgetState();
    }

    class _MyWidgetState extends State<MyWidget> {
    @override
    Widget build(BuildContext context) {
    // TODO: implement build
    return null;
    }

    void myMethodIWantToCallFromAnotherWidget() {
    print('calling myMethodIWantToCallFromAnotherWidget..');
    // actual implementation here
    }
    }

    Let’s create another widget from where we would be calling the myMethodIWantToCallFromAnotherWidget.

    class MyAnotherWidget extends StatelessWidget {

    @override
    Widget build(BuildContext context) {

    return Container(child: MyWidget());
    }

    void onSomeEvent() {
    // call myMethodIWantToCallFromAnotherWidget from MyWidget
    }
    }


    So, here we are trying to call the method of MyWidget from MyAnotherWidget. So how can we achieve this Flutter?

    Use GlobalKey
    In Flutter, GlobalKey helps to uniquely identify the created widgets. With the help of GlobalKey, we can access the State of Flutter widgets hence allowing us to call methods from any child widget.

    Now in order to use GlobalKey we need to make a few changes in our code. Firstly, we need to add a Key identifier to our MyWidget constructor.



    class MyWidget extends StatefulWidget {
    MyWidget({Key key}) : super(key: key);
    ...
    This allows us to pass a GlobalKey to identify the state of MyWidget when creating an instance of this widget.

    final GlobalKey<_MyWidgetState> _myWidgetState = GlobalKey<_MyWidgetState>();

    @override
    Widget build(BuildContext context) {
    return Container(child: MyWidget(key: _myWidgetState));
    }
    And finally, we can make use of this key as below:

    void onSomeEvent() {
    // call myMethodIWantToCallFromAnotherWidget from MyWidget
    _myWidgetState.currentState.myMethodIWantToCallFromAnotherWidget();
    }
    Congratulations you now know how to call methods of child widgets from parent widget!
 */

//class ItemData{
//  String Name;
//  int Counter;
//  bool ShouldVisible;
//
//  ItemData({
//    this.Name,
//    this.Counter,
//    this.ShouldVisible
//  });
//}
//
//List<ItemData> itemData = [
//  ItemData(
//      Name: 'Shoes 1',
//      Counter: 1,
//      ShouldVisible: false
//  ),
//  ItemData(
//      Name: 'Shoes 2',
//      Counter: 1,
//      ShouldVisible: false
//  ),];