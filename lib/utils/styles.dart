import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';


abstract class AppStyles {


  static const BorderSide _customBorderSide =  BorderSide(width: 1.0, color: Colors.grey,);
  static const  BoxDecoration boxDecoration = const BoxDecoration(
    border: Border(
      bottom:_customBorderSide,
      top: _customBorderSide,
      left:_customBorderSide,
      right:_customBorderSide,
    ),
    borderRadius: BorderRadius.all( Radius.circular(15.0)),

  );



  static const TextStyle smallText = TextStyle(
      fontFamily: 'Montserrat',
      color: Colors.grey,
      fontSize: 14,
      height: 1,
      fontWeight: FontWeight.w600
  );
  static const TextStyle blackSmallText = TextStyle(
      fontFamily: 'Montserrat',
      color: Colors.black,
      fontSize: 14,
      height: 1,
      fontWeight: FontWeight.w600
  );


  static const TextStyle greenSmallText = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.PRIMARY_COLOR,
      fontSize: 14,
      height: 1,
      fontWeight: FontWeight.w600
  );

  static const TextStyle greenBigText = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.PRIMARY_COLOR,
      fontSize: 16,
      height: 2,
      fontWeight: FontWeight.w600
  );



  static const TextStyle bigText = TextStyle(
      fontFamily: 'Montserrat',
      color: Colors.black,
      fontSize: 17,
      height: 2,
      fontWeight: FontWeight.w900
  );


  static const TextStyle largeText = TextStyle(
      fontFamily: 'Montserrat',
      color: Colors.black,
      fontSize: 23,
      height: 2,
      fontWeight: FontWeight.bold
  );

}