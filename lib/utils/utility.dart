import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:date_format/date_format.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';



class Utility {


  //get device width
  static double getDeviceWidth(BuildContext context){
    return MediaQuery.of(context).size.width;
  }

  //get device width
  static double getDeviceHeight(BuildContext context){
    return MediaQuery.of(context).size.height;
  }

  //check if email is valid
  static bool isValidEmail(String email){
    return true;
  }

  //validation for email
  static String validateEmail(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (email.isEmpty)
      return 'Email can\'t be empty';
    else if (!regex.hasMatch(email))
      return 'Enter valid email address';
    else
      return null;
  }

  String convertStringFromDate(DateTime todayDate ) {
    print(formatDate(todayDate, [yyyy, '-', mm, '-', dd]));
    return formatDate(todayDate, [yyyy, '-', mm, '-', dd]);
  }

  void convertDateFromString(String strDate){
    DateTime todayDate = DateTime.parse(strDate);
    print(todayDate);
    print(formatDate(todayDate, [yyyy, '/', mm, '/', dd]));
  }

  bool validateUsername(String value){
    String  pattern = r'^[a-zA-Z]+(\s[a-zA-Z]+)?$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }


  String validateMobile(String value) {
    String patttern =  r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (!regExp.hasMatch(value)) {
      return 'Invalid';
    }
    return "Valid";
  }

  bool validatePassword(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{2,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  void showMessage(String message,{bool error = false}){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: error ? Colors.red : Colors.blueAccent,
        textColor:  error ? Colors.white :Colors.white,
        fontSize: 16.0
    );
  }


  static void setStatusBar(){
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // this one for android
        statusBarBrightness: Brightness.light// this one for iOS
    ));
  }


  static String dateFormatter(DateTime date) {
    dynamic dayData =
        '{ "1" : "Mon", "2" : "Tue", "3" : "Wed", "4" : "Thur", "5" : "Fri", "6" : "Sat", "7" : "Sun" }';

    dynamic monthData =
        '{ "1" : "Jan", "2" : "Feb", "3" : "Mar", "4" : "Apr", "5" : "May", "6" : "June", "7" : "Jul", "8" : "Aug", "9" : "Sep", "10" : "Oct", "11" : "Nov", "12" : "Dec" }';

    return json.decode(dayData)['${date.weekday}'] +
        ", " +
        date.day.toString() +
        " " +
        json.decode(monthData)['${date.month}'] +
        " " +
        date.year.toString();
  }




}
