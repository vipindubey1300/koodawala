import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:koodawaala/utils/app_colors.dart';


class AddressItem extends StatefulWidget {
  final String title;
  final int index;
  final bool isSelected;
  Function(int) selectItem;

  AddressItem(
      this.selectItem, {
        Key key,
        this.title,
        this.index,
        this.isSelected,
      }) : super(key: key);

  _AddressItemState createState() => _AddressItemState();
}

class _AddressItemState extends State<AddressItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration:  BoxDecoration(
          border: Border.all(color: Colors.grey,width: 1.0, ),
          borderRadius: BorderRadius.all( Radius.circular(15.0)),
        ),
        child:Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircularCheckBox(
                value:widget.isSelected,
                checkColor: Colors.white  ,activeColor: Colors.green, inactiveColor: Colors.redAccent, disabledColor: Colors.grey,
                materialTapTargetSize: MaterialTapTargetSize.padded,
                onChanged: (bool x) {
                  widget.selectItem(widget.index);
                }
            ),

            Expanded(child:  Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text("Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs ipsum, or lipsum as it is sometimes known, is dummy text used in "),
                Text('₹1200',style: TextStyle(color: AppColors.PRIMARY_COLOR,fontWeight: FontWeight.w700,fontSize: 17))
              ],
            ))
          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
