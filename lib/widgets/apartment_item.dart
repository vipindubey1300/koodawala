import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/styles.dart';


class ApartmentItem extends StatefulWidget {
  final String title;
  final String price;
  final int index;
  final bool isSelected;
  final Function(int) selectItem;

  ApartmentItem(
      this.selectItem, {
        Key key,
        this.title,
        this.price,
        this.index,
        this.isSelected,
      }) : super(key: key);

  _ApartmentItemState createState() => _ApartmentItemState();
}

class _ApartmentItemState extends State<ApartmentItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration:  BoxDecoration(
          border: Border.all(color: Colors.grey,width: 1.0, ),
          borderRadius: BorderRadius.all( Radius.circular(15.0)),
        ),
        child:Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
           Row(
             children: [
               CircularCheckBox(
                   value:widget.isSelected,
                   checkColor: Colors.white  ,activeColor: Colors.green, inactiveColor: Colors.redAccent, disabledColor: Colors.grey,
                   materialTapTargetSize: MaterialTapTargetSize.padded,
                   onChanged: (bool x) {
                     widget.selectItem(widget.index);
                   }
               ),
               Text(widget.title,style: AppStyles.blackSmallText,),
             ],
           ),


            Text(widget.price),

          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
