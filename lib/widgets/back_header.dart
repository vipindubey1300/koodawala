import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';


class BackHeader extends StatelessWidget {
  BackHeader({@required this.text});
  final String text;

  @override
  Widget build (BuildContext context)  => Container(height: 50,color: AppColors.WHITE_COLOR_TRANSPARENT,padding: EdgeInsets.all(7),
      child: Row(
        children: <Widget>[
          SizedBox(width: 3,),
          InkWell(
            onTap:() => Navigator.of(context).pop(),
            child: Icon(Icons.arrow_back) ,
          ),
          SizedBox(width: 14,),
          Text(text)
        ],
      )
  );

}


