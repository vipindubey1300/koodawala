import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';
import 'package:koodawaala/utils/utility.dart';

class CustomButton extends StatelessWidget {
  CustomButton({@required this.text ,this.onPressed});
  final GestureTapCallback onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: Utility.getDeviceWidth(context) * 0.8,
      child:  RawMaterialButton(
        fillColor: AppColors.PRIMARY_COLOR,
        splashColor: Colors.greenAccent,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child:Text(text, maxLines: 1,style: TextStyle(color: Colors.white) ),
        ),
        onPressed: onPressed,
        shape: const StadiumBorder(),
      ),
    );
  }
}