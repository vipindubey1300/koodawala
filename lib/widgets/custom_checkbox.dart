
import 'package:flutter/material.dart';
import 'package:circular_check_box/circular_check_box.dart';


class CustomCheckBox extends StatefulWidget {
  final bool isSelected;
  final Function(bool) selectItem;
  CustomCheckBox({@required this.isSelected,this.selectItem});
  @override
  _CustomCheckBoxState createState() => new _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBox> {


  @override
  Widget build(BuildContext context) {
    return new   CircularCheckBox(
        value:widget.isSelected,
        checkColor: Colors.white  ,activeColor: Colors.green, inactiveColor: Colors.redAccent, disabledColor: Colors.grey,
        materialTapTargetSize: MaterialTapTargetSize.padded,
        onChanged: (bool x) {
            if (widget.selectItem != null) widget.selectItem(x);
        }
    );
  }
}

/**
 * USAGE --
 * parent class ->
 *
 *   new CustomCheckBox(isSelected: isChecked,)
 *   bool iss  = true;
    setState(() => isChecked = !isChecked);
*/