
import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:koodawaala/utils/styles.dart';
import 'package:koodawaala/utils/utility.dart';
import 'package:koodawaala/widgets/global/google_map.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';

class CustomMapView extends StatefulWidget {
  final Function(String) onGetAddress;
  const CustomMapView({Key key, this.onGetAddress}) : super(key: key);

  @override
  _CustomMapViewState createState() => _CustomMapViewState();
}

class _CustomMapViewState extends State<CustomMapView> {
  GoogleMapController map;
  String _userAddress;

  String getAddress(){
    if(_userAddress == null) return "";
    else return _userAddress;
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }


    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }


  void setGoogleMap(double lat,double long) {
    print('Map-----${map}');
    final CameraPosition _kLake = CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(lat, long),
        tilt: 59.440717697143555,
        zoom: 19.151926040649414
    );
    map?.animateCamera(CameraUpdate.newCameraPosition(_kLake)); //means if map is not null then map.animate

  }

  //init state should not use async so we use future void function here
  Future<void> initialize() async {
    Future<Position> pos = _determinePosition();

    pos.then((value) async {
          setGoogleMap(value.latitude,value.longitude);
          final coordinates = new Coordinates(value.latitude,value.longitude);
          var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          var temp = '${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine},'
              ' ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}';
         if(widget.onGetAddress != null) widget.onGetAddress(temp);
          if (!mounted) return; //this avoid memory leak due to setState after Screen Un-focus
          setState(() => _userAddress =  temp);

     })
     .catchError((onError){
       if(widget.onGetAddress != null) widget.onGetAddress('');
         Utility().showMessage(onError.toString(),error : true);
    });

    return null;
  }


  @override
  void initState()  {
    super.initState();
    //rest code here
    initialize();

  }

  @override
  void dispose() {
    //rest code here
    map = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: AppStyles.boxDecoration,
      height: Utility.getDeviceHeight(context) * 0.3,
      child:Stack(
        children: [
          Positioned(bottom: 0,left: 0,right: 0,top: 0,
              child: MapWidget(onMapCreated: (controller) =>  map = controller)
          ),

          Positioned(bottom: 0,left: 0,right: 0,
              child: Container(
                clipBehavior: Clip.antiAlias,
                padding: EdgeInsets.all(0),
                decoration: new BoxDecoration(
                    color: Colors.grey.shade200.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(12)
                ),
                height: Utility.getDeviceHeight(context) * 0.08,
                child:ClipRect(
                  child: BackdropFilter(
                    filter:new ImageFilter.blur(sigmaX: 10,sigmaY: 10),
                    child: Center(
                      child:Padding(padding: EdgeInsets.all(7),child:  Text(_userAddress ?? ''),),
                    ),
                  ),

                ),)
          )
        ],
      ),
    );
  }
}