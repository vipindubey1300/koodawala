
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class ProgressBar extends StatelessWidget {

  final Color color;
  //Indicator.ballPulse
  const ProgressBar({ Key key, @required this.color}) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
        height: 100,
        child: Center(
          child: LoadingIndicator(indicatorType: Indicator.ballPulse,color: color,),
        )
    );
  }
}