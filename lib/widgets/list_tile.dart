
import 'package:flutter/material.dart';
import 'package:koodawaala/utils/styles.dart';
import 'package:koodawaala/widgets/custom_checkbox.dart';
import 'package:koodawaala/widgets/quantity.dart';


class ListTileItem extends StatefulWidget {

  final String title;
  final int index;
  final bool isSelected;
  final Function(int) selectItem;

  ListTileItem(
      this.selectItem, {
        Key key,
        this.title,
        this.index,
        this.isSelected,
      }) : super(key: key);


  @override
  _ListTileItemState createState() => new _ListTileItemState();
}

class _ListTileItemState extends State<ListTileItem> {

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.all(8),
      decoration: AppStyles.boxDecoration,
      child:  new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: [
              CustomCheckBox(isSelected: widget.isSelected, selectItem: (bool value) => widget.selectItem(widget.index),),
              SizedBox(width: 10,),
              Text(widget.title),
            ],
          ),

          Quantity()
        ],
      ),

    );
  }
}