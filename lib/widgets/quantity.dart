
import 'package:flutter/material.dart';
import 'package:koodawaala/utils/app_colors.dart';


class Quantity extends StatefulWidget {
  final Key key;
  Quantity({this.key}): super(key: key);
  @override
  _QuantityState createState() => new _QuantityState();
}

class _QuantityState extends State<Quantity> {
  int _itemCount = 0;

  int getCurrentCount() => _itemCount;

  @override
  Widget build(BuildContext context) {
    return new Container(
      child:  new Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: ClipOval(
              child: Material(
                color: Colors.orangeAccent, // button color
                child: InkWell(
                  splashColor: Colors.red, // inkwell color
                  child: SizedBox(width: 30, height: 30, child: Icon(Icons.remove,size: 13,color: Colors.black,)),
                  onTap: () => setState(()=>_itemCount--),

                ),
              ),
            ),
          ),

          new Text(_itemCount.toString()),

          Padding(
              padding: EdgeInsets.all(10),
              child: ClipOval(
              child: Material(
                color: Colors.green, // button color
                child: InkWell(
                  splashColor: Colors.red, // inkwell color
                  child: SizedBox(width: 30, height: 30, child: Icon(Icons.add,size: 13,color: Colors.white,)),
                  onTap: () => setState(()=>_itemCount++),

                ),
              ),
            ),
          )


        ],
      ),
    );
  }
}

/**
 * ALTERNATIVE --->
 * RawMaterialButton(
    onPressed:()=> setState(()=>_itemCount--),
    elevation: 2.0,
    fillColor: Colors.orangeAccent,
    child: Icon(
    Icons.remove,
    size: 15.0,
    ),
    padding: EdgeInsets.all(0.0),
    shape: CircleBorder(),
    )
*/